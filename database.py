async def insert_or_update_users(self, user_id: int, leagues: str):
    user_leagues = await self.select_users(user_id)
    if user_leagues is not None:
        await self.update_users(user_id, leagues)
    else:
        await self.insert_users(user_id, leagues)
