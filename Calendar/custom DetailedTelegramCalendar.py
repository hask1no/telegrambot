from calendar import monthrange, hourCalendar
from datetime import date, datetime

from dateutil.relativedelta import relativedelta
from telegram_bot_calendar import DetailedTelegramCalendar

from Calendar.custom_base_calendar import *

STEPS = {YEAR: MONTH, MONTH: DAY, DAY: HOUR, HOUR: MINUTE}

PREV_STEPS = {MINUTE: HOUR, HOUR: DAY, DAY: MONTH, MONTH: YEAR, YEAR: YEAR}
PREV_ACTIONS = {MINUTE: GOTO, HOUR: HOUR, DAY: GOTO, MONTH: GOTO, YEAR: NOTHING}


class CustomDetailedCalendar(DetailedTelegramCalendar):
    first_step = YEAR

    def __init__(self, calendar_id=0, current_date=None, additional_buttons=None, locale='ru',
                 min_date=None,
                 max_date=None, telethon=False, **kwargs):
        super(DetailedTelegramCalendar, self).__init__(calendar_id, current_date=current_date,
                                                       additional_buttons=additional_buttons, locale=locale,
                                                       min_date=min_date, max_date=max_date, is_random=False,
                                                       telethon=telethon, **kwargs)

    def _build(self, step=None, **kwargs):
        if not step:
            step = self.first_step

        self.step = step
        if step == YEAR:
            self._build_years()
        elif step == MONTH:
            self._build_months()
        elif step == DAY:
            self._build_days()
        elif step == HOUR:
            self._build_hour()
        elif step == MINUTE:
            self._build_minute()

    def _process(self, call_data, *args, **kwargs):
        params = call_data.split("_")
        params = dict(
            zip(["start", "calendar_id", "action", "step", "year", "month", "day", "hour", "minute"]
                [:len(params)], params)
        )

        if params['action'] == NOTHING:
            return None, None, None, None, None
        step = params['step']

        year = int(params['year'])
        month = int(params['month'])
        day = int(params['day'])
        hour = int(params['hour'])
        minute = int(params['minute'])
        self.current_date = datetime(year, month, day, hour, minute)

        if params['action'] == GOTO:
            self._build(step=step)
            return None, self._keyboard, step

        if params['action'] == SELECT:
            if step in STEPS:
                self._build(step=STEPS[step])
                return None, self._keyboard, STEPS[step]
            else:
                return self.current_date, None, step

    def _build_hour(self, *args, **kwargs):
        hour_num = hourCalendar(self.current_date.month, self.current_date.day)[1]

        start = self.current_date.replace(hour=1)
        hours = self._get_period(HOUR, start, hour_num)
        hours_buttons = rows(
            [
                self._build_button(h.hour if h else self.empty_hour_button, SELECT if h else NOTHING, HOUR, h,
                                   is_random=self.is_random)
                for h in hours
            ],
            self.size_hour
        )
        nav_buttons = self._build_nav_buttons(HOUR, diff=relativedelta(hours=24),
                                              mind=max_date(start, HOUR),
                                              maxd=min_date(start.replace(hour=24), HOUR))

        self._keyboard = self._build_keyboard(hours_buttons + nav_buttons)

    def _build_minute(self, *args, **kwargs):
        minute_num = self.minuteCalendar(self.current_date.year, self.current_date.month)[1]

        start = self.current_date.replace(minute=1)
        minutes = self._get_period(MINUTE, start, minute_num)
        minute_buttons = rows(
            [
                self._build_button(m.minute if m else self.empty_minute_button, SELECT if m else NOTHING, MINUTE, m,
                                   is_random=self.is_random)
                for m in minutes
            ],
            self.size_minute
        )
        nav_buttons = self._build_nav_buttons(MINUTE, diff=relativedelta(minutes=30),
                                              mind=max_date(start, MINUTE),
                                              maxd=min_date(start.replace(minute=30), MINUTE))

        self._keyboard = self._build_keyboard(minute_buttons + nav_buttons)

    def _build_nav_buttons(self, step, diff, mind, maxd, *args, **kwargs):

        text = self.nav_buttons[step]

        sld = list(map(str, self.current_date.timetuple()[:5]))
        data = [sld[0], self.months[self.locale][int(sld[1]) - 1], sld[2]]
        data = dict(zip(["year", "month", "day", "hour", "minute"], data))
        prev_page = self.current_date - diff
        next_page = self.current_date + diff

        prev_exists = mind - relativedelta(**{LSTEP[step] + "s": 1}) >= self.min_date
        next_exists = maxd + relativedelta(**{LSTEP[step] + "s": 1}) <= self.max_date

        return [[
            self._build_button(text[0].format(**data) if prev_exists else self.empty_nav_button,
                               GOTO if prev_exists else NOTHING, step, prev_page, is_random=self.is_random),
            self._build_button(text[1].format(**data),
                               PREV_ACTIONS[step], PREV_STEPS[step], self.current_date, is_random=self.is_random),
            self._build_button(text[2].format(**data) if next_exists else self.empty_nav_button,
                               GOTO if next_exists else NOTHING, step, next_page, is_random=self.is_random),
        ]]

    def _get_period(self, step, start, diff, *args, **kwargs):
        if step != DAY:
            return super(DetailedTelegramCalendar, self)._get_period(step, start, diff, *args, **kwargs)

        dates = []
        cl = calendar.monthcalendar(start.year, start.month)
        for week in cl:
            for day in week:
                if day != 0 and self._valid_date(date(start.year, start.month, day)):
                    dates.append(date(start.year, start.month, day))
                else:
                    dates.append(None)

        return dates
