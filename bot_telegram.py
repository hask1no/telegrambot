import logging
import emoji

from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.webhook import DeleteMessage
from aiogram.types import *
from aiogram.dispatcher.filters import Text, state
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.utils import callback_data
from aiogram.utils.markdown import text
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP, RUSSIAN_STEP
from calendar import monthrange
import config
import sqlite3
# from aiogram.calendar import simple_cal_callback, SimpleCalendar, dialog_cal_callback, DialogCalendar

__connection = None


def get_connection():
    global __connection
    if __connection is None:
        __connection = sqlite3.Connection('base_client')
    return __connection


def init_db(force: bool = False):
    conn = get_connection()
    connect = conn.cursor()
    return connect


# Объект бота
bot = Bot(config.token)
# Диспетчер для бота
dp = Dispatcher(bot)
# Логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)


async def on_startup(_):
    print('Бот вышел в онлайн')


@dp.message_handler(commands=['start'])
async def process_start(message: types.Message):
    keyboard_start = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    first_buttons = [
        'Записаться на маникюр ' + emoji.emojize(':nail_polish:'),
        'Записаться на курс ' + emoji.emojize(':student:'),
        'Отменить запись ' + emoji.emojize(':neutral_face:')
    ]
    keyboard_start.add(*first_buttons)
    await bot.send_message(message.chat.id, 'Приветик, {0.first_name} '.format(message.from_user)
                           + emoji.emojize(':smiling_face_with_open_hands:'), reply_markup=keyboard_start)
    await message.answer(emoji.emojize('Выберите один из вариантов: :white_heart:'), reply_markup=keyboard_start)


@dp.message_handler(commands=['admin'])
async def admin_menu(message: types.Message):
    keyboard = InlineKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True, row_width=2)
    sql_query = f'SELECT id, telegram_id from masters where telegram_id = {message.chat.id}'
    connect = init_db()
    admin_id = connect.execute(sql_query)
    if admin_id not in config.telegram_id:
        admin = [
        ]
        keyboard.add(*admin)
        await bot.send_message(message.chat.id, 'Выбрать дату и время для выбора окошек на неделю: ',
                               reply_markup=keyboard)
    else:
        await message.answer(f'У тебя нет админки\n Куда полез {emoji.emojize(":grinning_face_with_sweat:")}')
        await message.delete()


# @dp.callback_query_handler()
# async def buttons_week(call: types.CallbackQuery):


@dp.message_handler(Text(equals='Записаться на маникюр ' + emoji.emojize(':nail_polish:')))
async def buttons_nail(call: types.CallbackQuery, offset: int = 0):
    keyboard = InlineKeyboardMarkup()
    manicure = [
        InlineKeyboardMarkup(text='Василий ' + emoji.emojize(':person:'), callback_data='num_Vasiliy'),
        InlineKeyboardMarkup(text='Кристина ' + emoji.emojize(':girl:'), callback_data='num_Kristina'),
        InlineKeyboardMarkup(text='Лена ' + emoji.emojize(':girl:'), callback_data='num_Lena')
    ]
    manicure_keys = [*manicure][0+offset:5+offset]
    await bot.send_message(call.from_user.id, 'Выберите мастера: ', reply_markup=keyboard)
    for i in manicure_keys:
        if i in manicure:
            await keyboard.add(InlineKeyboardButton(f"{emoji.emojize(':check_mark_button:')} {manicure[i]}",
                               callback_data=f'del_manicure_#{offset}#{i}'))
        else:
            keyboard.add(InlineKeyboardButton(manicure[i], callback_data=f'add_manicure_#{offset}#{i}'))
    return keyboard


@dp.callback_query_handler(text='num_Vasiliy')
async def buttons_vasiliy(call: types.CallbackQuery):
    button_vasiliy = [
        InlineKeyboardMarkup(text='Выбрать дату', callback_data='vasiliy_data'),
        InlineKeyboardMarkup(text='Инстаграм ' + emoji.emojize(':mobile_phone_with_arrow:'),
                             url='https://www.instagram.com/v_nurislamov_nail/', callback_data='vasiliy_instagram'),
        InlineKeyboardMarkup(text='Отзывы ' + emoji.emojize(':smiling_face_with_halo:'),
                             url='https://www.instagram.com/v_nurislamov_nail/',
                             callback_data='feedback_vasiliy'),
        InlineKeyboardMarkup(text='Назад ' + emoji.emojize(':BACK_arrow:'), callback_data='back')
    ]
    keyboard = InlineKeyboardMarkup()
    keyboard.add(*button_vasiliy)
    await call.message.answer('Выбери услугу Василия', reply_markup=keyboard)
    # тайминг подтверждения (часик)
    await call.answer()
    # убрать клавиатуру
    # await call.message.edit_reply_markup()
    # замена имени
    # if call.data == 'num_Vasiliy':
    #     await bot.send_message(call.from_user.id, 'Василий' + emoji.emojize(':package:'))


# def read_staff_schedule(ind):
#     c = """
#         INSERT INTO client VALUES ()
#     """

# def write_masters


# @dp.callback_query_handler(text='vasiliy_data')
# async def staff(call: types.CallbackQuery):
#     buttons_data = []
#     for read_staff_schedule() in :
#
#     keyboard = InlineKeyboardMarkup()
#     keyboard.add(*buttons_data)
#     await call.message.answer('', reply_markup=keyboard)
#     await call.answer()

@dp.message_handler(commands='calendar')
async def vasiliy_data(message: types.Message):
    calendar, step = DetailedTelegramCalendar().build()
    await bot.send_message(message.chat.id, f"Выбрать {RUSSIAN_STEP[step]}", reply_markup=calendar)


@dp.callback_query_handler(DetailedTelegramCalendar.func())
async def inline_kb_answer_callback_handler(query):
    result, key, step = DetailedTelegramCalendar().process(query.data)
    if not result and key:
        await bot.edit_message_text(f"Выбрать {RUSSIAN_STEP[step]}",
                                    query.message.chat.id,
                                    query.message.message_id,
                                    reply_markup=key)
    elif result:
        sql = "insert into staff_schedule {message.chat.id}"
        await bot.edit_message_text(f"Вы выбрали: {result}",
                                    query.message.chat.id,
                                    query.message.message_id)


@dp.callback_query_handler(text='num_Kristina')
async def buttons_kristina(call: types.CallbackQuery):
    button_kristina = [
        InlineKeyboardMarkup(text='Выбрать дату', callback_data='kristina_data'),
        InlineKeyboardMarkup(text='Инстаграм ' + emoji.emojize(':mobile_phone_with_arrow:'),
                             url='https://instagram.com/krylova_nails_ufa_?utm_medium=copy_link',
                             callback_data='kristina_instagram'),
        InlineKeyboardMarkup(text='Отзывы ' + emoji.emojize(':smiling_face_with_halo:'),
                             url='https://www.instagram.com/s/aGlnaGxpZ2h0OjE4MDI4NjA0OTc0MjM1MjUx?story_media_id='
                                 '2169835032829599672&utm_medium=copy_link',
                             callback_data='feedback_kristina'),
        InlineKeyboardMarkup(text='Назад ' + emoji.emojize(':BACK_arrow:'), callback_data='back')
    ]
    keyboard = InlineKeyboardMarkup()
    keyboard.add(*button_kristina)
    await call.message.answer('Выбери услугу Кристины', reply_markup=keyboard)

    # тайминг подтверждения (часик)
    await call.answer()
    # убрать клавиатуру
    await call.message.edit_reply_markup()


@dp.callback_query_handler(text='num_Lena')
async def buttons_lena(call: types.CallbackQuery):
    button_lena = [
        InlineKeyboardMarkup(text='Выбрать дату', callback_data='lena_data'),
        InlineKeyboardMarkup(text='Инстаграм ' + emoji.emojize(':mobile_phone_with_arrow:'),
                             url='https://www.instagram.com/lookyanova.nails/', callback_data='lena_instagram'),
        InlineKeyboardMarkup(text='Отзывы ' + emoji.emojize(':smiling_face_with_halo:'),
                             url='https://www.instagram.com/s/aGlnaGxpZ2h0OjE4MDY5NTM2NTY5MDk2OTI4?story_media_id='
                                 '2168364129550202212_17568681876&utm_medium=copy_link',
                             callback_data='feedback_lena'),
        InlineKeyboardMarkup(text='Назад ' + emoji.emojize(':BACK_arrow:'), callback_data='back')
    ]
    keyboard = InlineKeyboardMarkup()
    keyboard.add(*button_lena)
    await call.message.answer('Выбери услугу Лены', reply_markup=keyboard)
    # тайминг подтверждения (часик)
    await call.answer()
    # убрать клавиатуру
    await call.message.edit_reply_markup()


"""
Возврать назад
"""


@dp.callback_query_handler(text='back')
async def button_back(call: CallbackQuery):
    await call.message.delete()
    # return DeleteMessage(message.chat.id)


"""
Ветвь для записи на курс
:return:
"""


@dp.message_handler(Text(equals='Записаться на курс ' + emoji.emojize(':student:')))
async def buttons1_study(message: types.Message):
    buttons_study = [
        InlineKeyboardMarkup(text='Базовый курс', callback_data='base'),
        InlineKeyboardMarkup(text='Повышение квалификации', callback_data='study'),
        InlineKeyboardMarkup(text='Отзывы', url='https://www.instagram.com/s/aGlnaGxpZ2h0OjE3OTE4MTI5NjI2NzQ3NDI1?'
                                                'story_media_id=2611986344725210324&utm_medium=copy_link',
                             callback_data='otzyv'),
        InlineKeyboardMarkup(text='Процесс обучения', url='https://www.instagram.com/s/aGlnaGxpZ2h0OjE3OTI3MzA5MDAyMjY'
                                                          '2Mjc3?story_media_id=2657980914610510348&utm_medium'
                                                          '=copy_link', callback_data='Process'),
        InlineKeyboardMarkup(text='Работа учениц', url='https://www.instagram.com/explore/tags/'
                                                       '%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D1%86%D1%8B_%D0%BE%D0%B2%D1%87'
                                                       '%D0%B8%D0%BD%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2%D0%BE%D0%B9_%D0%B5/',
                             callback_data='work_study'),
        InlineKeyboardMarkup(text='Мой инстаграм' + emoji.emojize(':mobile_phone_with_arrow:'),
                             url='https://www.instagram.com/nail_ufa_nail/'),
        InlineKeyboardMarkup(text='Назад ' + emoji.emojize(':BACK_arrow:'), callback_data='back')
    ]
    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(*buttons_study)
    await bot.send_message(message.chat.id, 'Супер, будем повышать твои знания ' + emoji.emojize(':woman_teacher:'),
                           reply_markup=keyboard)


@dp.callback_query_handler(text='base')
async def base_button(call: CallbackQuery):
    photo_base1 = open('./Photo/Base.jpg', 'rb')
    photo_base2 = open('./Photo/3.jpg', 'rb')
    photo_base3 = open('./Photo/4.jpg', 'rb')
    await bot.send_photo(call.message.chat.id, photo_base1)
    await bot.send_photo(call.message.chat.id, photo_base2)
    await bot.send_photo(call.message.chat.id, photo_base3)
    await bot.send_message(call.message.chat.id, 'Выбрать дату и время: ')


@dp.callback_query_handler(text='study')
async def study_button(call: CallbackQuery):
    photo_study1 = open('./Photo/study.jpg', 'rb')
    photo_study2 = open('./Photo/1.jpg', 'rb')
    photo_study3 = open('./Photo/2.jpg', 'rb')
    await bot.send_photo(call.message.chat.id, photo_study1)
    await bot.send_photo(call.message.chat.id, photo_study2)
    await bot.send_photo(call.message.chat.id, photo_study3)
    await bot.send_message(call.message.chat.id, 'Выбрать дату и время: ')


@dp.message_handler(Text(equals='Отменить запись ' + emoji.emojize(':neutral_face:')))
async def buttons2_cancellation(message: types.Message):
    # if message.chat.id == "bd client orders telegram_id"
    buttons_cancellation = [
        InlineKeyboardMarkup(text='Отменить запись на курс', callback_data='cancel_kurs'),
        InlineKeyboardMarkup(text='Отменить запись на маникюр', callback_data='cancel_manicur'),
        InlineKeyboardMarkup(text='Назад ' + emoji.emojize(':BACK_arrow:'), callback_data='back')
    ]
    keyboard = InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons_cancellation)
    await bot.send_message(message.chat.id, 'Очень жаль что вы не попадете в нашу прекрасную студию '
                           + emoji.emojize(':confused_face:'), reply_markup=keyboard)


# Произваольный текст
@dp.message_handler()
async def empty(message: types.Message):
    await message.answer('Нет такой комманды')
    await message.delete()
    await bot.send_message(message.chat.id, 'Воспользоваться коммандой - /start')


# Произвольныое фото
@dp.message_handler()
async def empty_photo(message):
    await bot.send_message(message.chat.id, 'Воспользоваться коммандой - /start')


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
